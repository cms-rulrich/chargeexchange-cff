import FWCore.ParameterSet.Config as cms
import CommonFSQFramework.Core.Util
import os

isData = True


if "TMFSampleName" not in os.environ:
    print "TMFSampleName not found, assuming we are running on MC"
else:
    s = os.environ["TMFSampleName"]
    sampleList=CommonFSQFramework.Core.Util.getAnaDefinition("sam")
    isData =  sampleList[s]["isData"]
    if isData: print "Disabling MC-specific features for sample",s
        

# this is important to get the right trigger setup
from Configuration.StandardSequences.Eras import eras

# Modify for if the phase 1 pixel detector is active
#if eras.phase1Pixel.isChosen() :
 #   muonMonitors.remove(muonAnalyzer)

process = cms.Process('ChargeExchange', eras.Run2_2016)

process.load("FWCore.MessageService.MessageLogger_cfi")
#process.MessageLogger.cerr = cms.untracked.PSet(placeholder = cms.untracked.bool(True))
process.MessageLogger.cerr.FwkReport.reportEvery = 1000000
process.MessageLogger.suppressError = cms.untracked.vstring('EcalRecHitProducer','EcalLaserDbService','EcalLaserCorrectionService','Ecal*')
#process.MessageLogger = cms.Service("MessageLogger",
#                     destinations   = cms.untracked.vstring('messages')
#                    destinations       =  cms.untracked.vstring('messages.txt'),
#                     debugModules   = cms.untracked.vstring('*'),
#                     messages          = cms.untracked.PSet(
#                                                threshold =  cms.untracked.string('DEBUG')
#                                                    ),
#                    suppressDebug  = cms.untracked.vstring('wordyModule'),
#                    suppressInfo       = cms.untracked.vstring('verboseModule', 'otherModule'),
#                  suppressError = cms.untracked.vstring('EcalRecHitProducer'),
#                  suppressDebug = cms.untracked.vstring('EcalRecHitProducer'),
#                  suppressInfo = cms.untracked.vstring('EcalRecHitProducer'),
#   cerr           = cms.untracked.PSet(
#                       threshold  = cms.untracked.string('WARNING') 
#        )
#)

process.load("Configuration.StandardSequences.MagneticField_cff")
process.load('Configuration.StandardSequences.Services_cff')
process.load('Configuration.EventContent.EventContent_cff')
process.load('Configuration.StandardSequences.MagneticField_AutoFromDBCurrent_cff')
process.load('Configuration.StandardSequences.RawToDigi_Data_cff')
# process.load("CondCore.CondDB.e_cfi")

process.maxEvents = cms.untracked.PSet(input = cms.untracked.int32(100))
process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(True),
    SkipEvent = cms.untracked.vstring('ProductNotFound'),
)





# xrootd-cms.infn.it
# cms-xrd-global.cern.ch
# cmsxrootd.fnal.gov
# Source
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
#       'root://cms-xrd-global.cern.ch//store/hidata/PARun2016D/ZeroBias/AOD/PromptReco-v1/000/286/518/00000/CCDC85D5-B9BD-E611-8260-FA163EE14920.root'
        'root://cms-xrd-global.cern.ch//store/hidata/PARun2016D/PAMinimumBias1/AOD/PromptReco-v1/000/286/520/00000/CE0E610B-D9BD-E611-B9CB-02163E011C5F.root'
    )
)


# Geometry and Detector Conditions
process.load("Configuration.Geometry.GeometryRecoDB_cff")
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '80X_dataRun2_Prompt_v15', '') 

###################################################################################

# get custom CASTOR conditions to mark/remove bad channels
# process.load("CondCore.DBCommon.CondDBSetup_cfi")
# process.CastorDbProducer = cms.ESProducer("CastorDbProducer")

# process.es_ascii = cms.ESSource("CastorTextCalibrations",
#    input = cms.VPSet(
#        cms.PSet(
#             object = cms.string('Gains'),
#             file = cms.FileInPath('data/customcond/castor/signals_in_fC.txt')
#         ),
#        cms.PSet(
#            object = cms.string('ChannelQuality'),
#            file = cms.FileInPath('data/customcond/castor/all_channels_good.txt')
#        ),
# #        cms.PSet(
# #            object = cms.string('Pedestals'),
# #            file = cms.FileInPath('data/customcond/castor/ConditionsFile_fC_Run262950_MuHV.txt')
# #        ),
#    )
# )
# process.es_prefer_castor = cms.ESPrefer('CastorTextCalibrations','es_ascii') 






#########################################################################################

#in data produce Tracker Rechits
#if isData:
#  process.PixelRecHits = cms.Path(process.siPixelRecHits)
#process.StripMatchedRecHits = cms.Path(process.siStripMatchedRecHits)

# HLT path filter
import HLTrigger.HLTfilters.hltHighLevel_cfi
process.TriggerFilter = HLTrigger.HLTfilters.hltHighLevel_cfi.hltHighLevel.clone(
    TriggerResultsTag = cms.InputTag("TriggerResults", "", "HLT"),
    #HLTPaths = ['HLT_PARandom_v*'],
    #HLTPaths = ['HLT_PAZeroBias_v*'],
    HLTPaths = ['*'],
    throw = True ## NNED THis FOR 2015 June since the HLT trigger was renamed!
    )




# Here starts the CFF specific part
import CommonFSQFramework.Core.customizePAT
process = CommonFSQFramework.Core.customizePAT.customize(process)

# GT customization
process = CommonFSQFramework.Core.customizePAT.customizeGT(process)

# define treeproducer
process.CFFTree = cms.EDAnalyzer("CFFTreeProducer")

import CommonFSQFramework.Core.CastorViewsConfigs
import CommonFSQFramework.Core.TriggerResultsViewsConfigs
import CommonFSQFramework.Core.VerticesViewsConfigs
import CommonFSQFramework.Core.RecoTrackViewsConfigs
import CommonFSQFramework.Core.CaloTowerViewsConfigs
import CommonFSQFramework.Core.PFObjectsViewsConfigs
import CommonFSQFramework.Core.ZDCViewsConfigs


process.CFFTree._Parameterizable__setParameters(CommonFSQFramework.Core.CastorViewsConfigs.get(["CastorRecHitViewFull"]))
process.CFFTree._Parameterizable__setParameters(CommonFSQFramework.Core.TriggerResultsViewsConfigs.get(["CastorPATriggerResultsView","L1GTriggerResultsView"]))
# process.CFFTree._Parameterizable__setParameters(CommonFSQFramework.Core.VerticesViewsConfigs.get(["PixelView"]))
process.CFFTree._Parameterizable__setParameters(CommonFSQFramework.Core.RecoTrackViewsConfigs.get(["RecoTrackView"]))
#process.CFFTree._Parameterizable__setParameters(CommonFSQFramework.Core.CaloTowerViewsConfigs.get(["CaloTowerView"]))
process.CFFTree._Parameterizable__setParameters(CommonFSQFramework.Core.PFObjectsViewsConfigs.get(["PFCandidateView"]))
process.CFFTree._Parameterizable__setParameters(CommonFSQFramework.Core.ZDCViewsConfigs.get(["ZDCRecHitView"]))



process.FiltererdTree = cms.Path(process.TriggerFilter*process.CFFTree)

process = CommonFSQFramework.Core.customizePAT.addPath(process, process.FiltererdTree)
