anaVersion="ChargeExchange_17092019"
anaType="ChargeExchange"

cbSmartCommand="smartCopy"
cbSmartBlackList=""
cbWMS="https://wmscms.cern.ch:7443/glite_wms_wmproxy_server"
skimEfficiencyMethod="getSkimEff"

sam = {}


# CFF Skim using interfill data from run PARun2016B
# pPbRun on RAW data 

sam["data_ALICERun_2016"] = {}
sam["data_ALICERun_2016"]["crabJobs"] = 232
sam["data_ALICERun_2016"]["GT"] = '80X_dataRun2_Prompt_v15'
sam["data_ALICERun_2016"]["name"] = 'data_ALICERun_2016'
sam["data_ALICERun_2016"]["treeName"]='CFFTree'
sam["data_ALICERun_2016"]["isData"] = True
sam["data_ALICERun_2016"]["numEvents"] = -1
sam["data_ALICERun_2016"]["pathSE"] = 'srm://dcache-se-cms.desy.de:8443/srm/managerv2?SFN=/pnfs/desy.de/cms/tier2/store/user/rulrich/ALICERun2016/PACastor/MuonAna_CastorMuons_data_ALICERun_2016/170902_235259'
sam["data_ALICERun_2016"]["pathTrees"] = '@CFF_LOCALTreeDIR@/PARun2016/ALICERun2016/PACastor/MuonAna_CastorMuons_data_ALICERun_2016/170902_235259'
sam["data_ALICERun_2016"]["json"] = 'ALICERun2016.json'
sam["data_ALICERun_2016"]["lumiMinBias"] = -1
sam["data_ALICERun_2016"]["XS"] = -1
sam["data_ALICERun_2016"]["DS"] = '/PACastor/PARun2016B-v1/RAW'

sam["data_PARun2016D_ChargeExchange_HLTRandom"] = {}
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["treeName"]='CFFTree'
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["crabJobs"] = 0
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["GT"] = '80X_dataRun2_Prompt_v15'
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["name"] = 'data_PARun2016D_ChargeExchange_HLTRandom'
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["isData"] = True
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["numEvents"] = -1
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["pathSE"] = 'srm://dcache-se-cms.desy.de:8443/srm/managerv2?SFN=/pnfs/desy.de/cms/tier2/store/user/rulrich/PARun2016D/ZeroBias/ChargeExchange_17092019_data_PARun2016D_ChargeExchange_HLTRandom/190917_103953/0000/'
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["pathTrees"] = '@CFF_LOCALTreeDIR@/PARun2016/PARun2016D/ZeroBias/ChargeExchange_data_PARun2016D_ChargeExchange_17092019_HLTRandom/190917_103953/0000'
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["json"] = 'PARun2016D.json'
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["lumiMinBias"] = -1
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["XS"] = -1
sam["data_PARun2016D_ChargeExchange_HLTRandom"]["DS"] = '/ZeroBias/PARun2016D-PromptReco-v1/AOD'

sam["data_PARun2016D_ChargeExchange_HLTZeroBias"] = {}
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["treeName"]='CFFTree'
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["crabJobs"] = 0
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["GT"] = '80X_dataRun2_Prompt_v15'
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["name"] = 'data_PARun2016D_ChargeExchange_HLTZeroBias'
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["isData"] = True
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["numEvents"] = -1
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["pathSE"] = 'srm://dcache-se-cms.desy.de:8443/srm/managerv2?SFN=/pnfs/desy.de/cms/tier2/store/user/rulrich/PARun2016D/ZeroBias/ChargeExchange_17092019_data_PARun2016D_ChargeExchange_HLTZeroBias/190917_104024/0000/'
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["pathTrees"] = '@CFF_LOCALTreeDIR@/PARun2016/PARun2016D/ZeroBias/ChargeExchange_data_PARun2016D_ChargeExchange_17092019_HLTZeroBias/190917_104024/0000'
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["json"] = 'PARun2016D.json'
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["lumiMinBias"] = -1
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["XS"] = -1
sam["data_PARun2016D_ChargeExchange_HLTZeroBias"]["DS"] = '/ZeroBias/PARun2016D-PromptReco-v1/AOD'

sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"] = {}
sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"]["crabJobs"] = 0
sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"]["GT"] = '80X_dataRun2_Prompt_v15'
sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"]["name"] = 'data_PARun2016D_ChargeExchange_PAMinimumBias1'
sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"]["isData"] = True
sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"]["numEvents"] = -1
sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"]["pathSE"] = 'srm://dcache-se-cms.desy.de:8443/srm/managerv2?SFN=/pnfs/desy.de/cms/tier2/store/user/rulrich/PARun2016D/PAMinimumBias1/ChargeExchange_17092019_data_PARun2016D_ChargeExchange_PAMinimumBias1/190917_103854/0000'
sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"]["pathTrees"] = '@CFF_LOCALTreeDIR@/PARun2016/PARun2016D/PAMinimumBias_17092019/ChargeExchange_data_PARun2016D_ChargeExchange_PAMinimumBias1/190917_103854/0000'
sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"]["json"] = 'Cert_286513-286520_HI5TeV_PromptReco_Pbp_Collisions16_JSON_noL1T.txt'
sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"]["lumiMinBias"] = -1
sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"]["XS"] = -1
sam["data_PARun2016D_ChargeExchange_PAMinimumBias1"]["DS"] = '/PAMinimumBias1/PARun2016D-PromptReco-v1/AOD'


# 2016B, ZeroBias data
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]={}
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["crabJobs"]=0
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["GT"]='80X_dataRun2_Prompt_v15'
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["treeName"] = 'CFFTree'
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["name"]='data_ZeroBias_PARun2016B_PromptReco_v1'
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["isData"]=True
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["numEvents"]=-1
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["pathSE"]='srm://dcache-se-cms.desy.de:8443/srm/managerv2?SFN=/pnfs/desy.de/cms/tier2/store/user/rulrich/PARun2016/ZeroBias/ZeroBias_data_ZeroBias_PARun2016B_PromptReco_v1/180302_161959'
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["pathTrees"]='@CFF_LOCALTreeDIR@/PARun2016B-v2/ZeroBias/ZeroBias_PARun2016B_PromptReco_v1/'
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["json"]='Cert_285090-285368_HI5TeV_PromptReco_pPb_Collisions16_TrackerCaloBarrel_JSON.txt'
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["lumiMinBias"]=-1
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["XS"]=-1
sam["data_ZeroBias_PARun2016B_PromptReco_v1"]["DS"]='/ZeroBias/PARun2016B-PromptReco-v1/AOD'


# sample from Michael,Lev,Igor, HIN-14-014

sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]={}
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["crabJobs"]=0
#sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["GT"]='GR_R_75_V5A'
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["GT"]='GR_P_V43F::All'
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["name"]='data_ReggeGribovMCfix_EposLHC_5TeV_pPb'
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["treeName"] = 'CFFTree'
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["isData"]=False
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["numEvents"]=-1
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["pathSE"]='srm://dcache-se-cms.desy.de:8443/srm/managerv2?SFN=/pnfs/desy.de/cms/tier2/store/user/rulrich/PARun2013-v2/ReggeGribovPartonMCfix_EposLHC_5TeV_pPb/ZeroBias_data_ReggeGribovMCfix_EposLHC_5TeV_pPb/180302_150601'
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["pathTrees"]='@CFF_LOCALTreeDIR@/MCpPb5TeV-v2/EPOS/'
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["json"]=''
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["lumiMinBias"]=-1
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["XS"]=-1
sam["data_ReggeGribovMCfix_EposLHC_5TeV_pPb"]["DS"]='/ReggeGribovPartonMCfix_EposLHC_5TeV_pPb/HiWinter13-pa_STARTHI53_V25-v1/GEN-SIM-RECO'


# sample from Merijn, FSQ-17-001

sam["data_Merijn_pPb_EposLHC_5TeV_measured"]={}
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["crabJobs"]=0
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["GT"]='GR_P_V43F::All'
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["name"]='data_Merijn_pPb_EposLHC_5TeV_measured'
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["treeName"] = 'CFFTree'
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["isData"]=False
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["numEvents"]=-1
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["pathSE"]='srm://dcache-se-cms.desy.de:8443/srm/managerv2?SFN=/pnfs/desy.de/cms/tier2/store/user/rulrich/PARun2013-v2/EPOS_pA_MeasuredPos_PROPSHIFTUNIT_10M_4C/ZeroBias_data_Merijn_pPb_EposLHC_5TeV_measured/180313_205910/0000/'
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["pathTrees"]='@CFF_LOCALTreeDIR@/MCpPb5TeV-v2/EposLHC_measured/'
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["json"]=''
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["lumiMinBias"]=-1
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["XS"]=-1
sam["data_Merijn_pPb_EposLHC_5TeV_measured"]["DS"]='/EPOS_pA_MeasuredPos_PROPSHIFTUNIT_10M_4C/mvandekl-RECO_RECOSIMoutput-ba277641c9661c475b21ced7d6287dd5/USER'


# this is the 2015 Run2015A Noise data from "empty BX" events. 

sam["data_MinimumBias_2015_JuneNoise"]={}
sam["data_MinimumBias_2015_JuneNoise"]["crabJobs"]=232
sam["data_MinimumBias_2015_JuneNoise"]["GT"]='80X_dataRun2_v13'
sam["data_MinimumBias_2015_JuneNoise"]["name"]='data_MinimumBias_2015_JuneNoise'
sam["data_MinimumBias_2015_JuneNoise"]["isData"]=True
sam["data_MinimumBias_2015_JuneNoise"]["numEvents"]=-1
sam["data_MinimumBias_2015_JuneNoise"]["pathSE"]='srm://grid-srm.physik.rwth-aachen.de:8443/srm/managerv2?SFN=/pnfs/physik.rwth-aachen.de/cms/store/user/rulrich/CastorNoise/EmptyBX/NoiseAna_Castor_data_MinimumBias_2015_JuneNoise/170902_232917'
sam["data_MinimumBias_2015_JuneNoise"]["pathTrees"]='@CFF_LOCALTreeDIR@/CastorNoise/EmptyBX/NoiseAna_Castor_data_MinimumBias_2015_JuneNoise/170902_232917'
sam["data_MinimumBias_2015_JuneNoise"]["json"]='muon_2015_0.0T_RUN247920.json'
sam["data_MinimumBias_2015_JuneNoise"]["lumiMinBias"]=-1
sam["data_MinimumBias_2015_JuneNoise"]["XS"]=-1
#sam["data_MinimumBias_2015_JuneNoise"]["pathPAT"]='/XXXTMFPAT/store/user/makbiyik/CastorMuon2015/muon_2015_JuneNoise//'
#sam["data_MinimumBias_2015_JuneNoise"]["DS"]='/EmptyBX/Run2015A-27Jan2016-v1/MINIAOD' # NO castor at all in miniaod ?????
sam["data_MinimumBias_2015_JuneNoise"]["DS"]='/EmptyBX/Run2015A-v1/RAW'
